import requests
import re
import execjs


def airchinalogin(name, passwd):
    node = execjs.compile(open('rusa.js', 'r').read())
    url2 = 'https://et.airchina.com.cn/www/servlet/com.ace.um.userLogin.servlet.B2CUserLogin'
    url1 = 'https://et.airchina.com.cn/www/jsp/userManager/login.jsp'

    s = requests.Session()
    s.headers = {
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "zh,zh-CN;q=0.9",
        "Cache-Control": "max-age=0",
        "Connection": "keep-alive",
        "Cookie": "JSESSIONID=152D59EF4E282D99AF8ADBE1156F2455; BIGipServerWeb_http=1275731628.20480.0000; _gcl_au=1.1.2095260977.1568901366; _Jo0OQK=449D1E47B36702FFDCD3888B63119547DF9E9B182B441BE005D374378279DADFFC5B32CE71AFD3BF1C938A9FE16103E31BD49CDACB1B3410A44CA345F6349CF18E1CD235FC374DFA3359CDD5576E997203F9CDD5576E997203FE579DEFA02689433DCD60C1999F0B880GJ1Z1Ww==; mbox=session#1568903872616-392017#1568908167|check#true#1568906367; s_pers=%20s_fid%3D418F44C0228AEE57-0009D42DF47453C6%7C1726759106905%3B; s_sess=%20s_sq%3D%3B%20s_cc%3Dtrue%3B",
        "Host": "et.airchina.com.cn",
        "Referer": "http://et.airchina.com.cn/www/jsp/userManager/myinfo.jsp",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36",
    }
    html = s.get(url1,verify=False)

    img_url = re.search(r'id="imgObj".*?src="(.*?)" />', html.text).group(1)

    # 验证码 图片
    img = s.get('https://et.airchina.com.cn' + img_url,verify=False)

    with open('yz.png', 'wb') as f:
        f.write(img.content)
    n = node.call('get_string', name)
    p = node.call('get_string', passwd)
    yzm = input('请输入验证码: ')
    data = {
        "typeselect": "手机号",
        "userFlag": "3",
        "userName": n,
        "password": p,
        "ewjy": yzm,
        "directURL": "",
        "userLoginType": "3",
        "userLoginFlag": "userLoginFlag",
    }
    s.post(url2, data=data)


