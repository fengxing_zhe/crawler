import json
import re
import time
import base64
import requests
from selenium import webdriver
from bs4 import BeautifulSoup
from selenium.webdriver.support.select import Select


class Get_airchina(object):
    def __init__(self):
        self.head = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) " \
                          "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36",
                     "Referer": "http://www.airchina.com.cn/cn/specials/routes_promotion.shtml"
                     }
        self.url = 'http://www.airchina.com.cn/cn/specials/routes_promotion.shtml' # 国内促销航线
        self._url = "http://et.airchina.com.cn/dynamicAd/GetAdvert//AirChinaFlightMapSearchPageSpecials_zh_CN.html?origin=%s&callback=jQuery18006382913692194427_1583564556867&_=%s"

    def get_city_code(self):
        driver = webdriver.PhantomJS()
        driver.get(self.url)
        sel = driver.find_element_by_css_selector('#originCity')
        city_list = Select(sel).options
        ret = {}
        for city in city_list:
            ret[city.get_attribute("value")] = city.text
        return ret

    def get_ret(self,ret):
        soup = BeautifulSoup(ret, 'lxml')
        informations = soup.findAll(class_="tableTdSearch")
        stttr = ''
        for i in informations:
            stttr += i.text
            try:
                href = i.a.get('href')
                stttr += href
            except:
                continue
            else:
                stttr += '\n'
        return stttr

    def run(self):
        city_dict =self.get_city_code()
        time_stamp = int(time.time())
        f = open('airchina','w',encoding='utf-8')
        for code in city_dict.keys():
            url = self._url % (code, time_stamp)
            response = requests.get(url, headers=self.head)
            ret = base64.b64decode(json.loads(re.search('\{.*\}', response.text).group()).get('data')).decode('utf-8')
            info = self.get_ret(ret)
            f.write(info)
            time.sleep(5)
        f.close()



if __name__ == '__main__':
    a = Get_airchina()
    a.run()
