# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy

from scrapy.loader import ItemLoader
from scrapy.loader.processors import MapCompose, TakeFirst


# class FilmItemLoader(ItemLoader):
#     default_selector_class = TakeFirst()


def get_zhuyan(_zhuyan):
    c = len(_zhuyan.strip())
    if c > 1:
        return _zhuyan.strip()


class get_zhuyan_out(object):
    def __call__(self, values):
        return  ' '.join(values).replace("'",'')


class get_film_intrduce_out(object):
    def __call__(self, values):
        return  values[0].replace(' ','')


class Film(scrapy.Item):
    film_name = scrapy.Field(input_processor=MapCompose(lambda x: x.strip()), output_processor=TakeFirst())
    daoyan = scrapy.Field(input_processor=MapCompose(lambda x: x.strip()), output_processor=TakeFirst())
    film_intrduce = scrapy.Field(input_processor=MapCompose(lambda x: x.strip().replace('\n','')), output_processor=get_film_intrduce_out())

    zhuyan = scrapy.Field(input_processor=MapCompose(get_zhuyan), output_processor=get_zhuyan_out())

class DB_Film(Film):
    url = scrapy.Field(output_processor=TakeFirst())
    rate = scrapy.Field(output_processor=TakeFirst())
    cover = scrapy.Field(output_processor=TakeFirst())
    id = scrapy.Field(output_processor=TakeFirst())